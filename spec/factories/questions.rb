FactoryBot.define do
  factory :question do
    user
    message { Faker::Lorem.paragraph }
  end
end
