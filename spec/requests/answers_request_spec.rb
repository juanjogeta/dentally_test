require "rails_helper"

describe "/answers", type: :request do
  describe "GET /questions/<question_id>/answers/new" do
    it "returns and empty answer instance" do
      user = create(:user)
      sign_in user

      question = create(:question)

      get new_question_answer_url(question)

      expect(response).to have_http_status(:success)
      assert_template "answers/new"
    end
  end

  describe "POST /questions/<question_id>/answers" do
    context "when message mandatory parameter is not present" do
      it "returns :unprocessable_entity status and doesn't create a new instance" do
        question = create(:question)
        user = create(:user)
        sign_in user

        expect { post question_answers_url(question), params: { answer: { foo: "Bar" } } }
          .not_to change(question.answers, :count)

        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context "when all the mandatory parameters are present" do
      it "creates a new instance and returns created status" do
        question = create(:question)
        user = create(:user)
        sign_in user

        expect { post question_answers_url(question), params: { answer: { message: "Foo" } } }
          .to change { question.answers.count }

        expect(response).to have_http_status(:created)
      end
    end
  end
end
