require "rails_helper"

describe "/questions", type: :request do
  describe "GET /questions" do
    it "returns all the questions" do
      question = create(:question)

      get questions_url, params: {}

      expect(response).to have_http_status(:success)
      assert_equal [question], assigns(:questions)
      assert_template "questions/index"
    end
  end

  describe "GET /questions/<question_id>" do
    it "returns the question" do
      question = create(:question)

      get question_url(question)

      expect(response).to have_http_status(:success)
      assert_equal question, assigns(:question)
      assert_template "questions/show"
    end
  end

  describe "GET /questions/new" do
    it "returns and empty question instance" do
      user = create(:user)
      sign_in user

      get new_question_url

      expect(response).to have_http_status(:success)
      assert_template "questions/new"
    end
  end

  describe "POST /questions" do
    context "when message mandatory parameter is not present" do
      it "returns :unprocessable_entity status and doesn't create a new instance" do
        user = create(:user)
        sign_in user

        expect { post questions_url, params: { question: { foo: "Bar" } } }
          .not_to change(Question, :count)

        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context "when all the mandatory parameters are present" do
      it "creates a new instance and returns created status" do
        user = create(:user)
        sign_in user

        expect { post questions_url, params: { question: { message: "Foo" } } }
          .to change { Question.count }

        expect(response).to have_http_status(:created)
      end
    end
  end

  describe "GET /questions/<question_id>/edit" do
    it "returns and empty question instance" do
      user = create(:user)
      sign_in user

      question = create(:question)

      get edit_question_url(question)

      expect(response).to have_http_status(:success)
      assert_equal question, assigns(:question)
      assert_template "questions/edit"
    end
  end

  describe "PUT /questions/<question_id>" do
    context "when question can't be updated" do
      it "returns :unprocessable_entity status and doesn't update the instance" do
        user = create(:user)
        sign_in user

        question = create(:question, message: "Foo", user: user)

        allow_any_instance_of(Question).to receive(:update).and_return(false)

        expect { put question_url(question), params: { question: { foo: "Bar" } } }
          .not_to change(question, :message)

        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context "when the owner is not the question owner" do
      it "returns :unprocessable_entity status and doesn't update the instance" do
        user = create(:user)
        sign_in user

        question = create(:question, message: "Foo", user: create(:user))

        expect { put question_url(question), params: { question: { message: "Bar" } } }
          .not_to change(question, :message)

        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context "when all the mandatory parameters are present" do
      it "creates a new instance and returns created status" do
        user = create(:user)
        sign_in user

        question = create(:question, message: "Foo", user: user)

        expect { put question_url(question), params: { question: { message: "Bar" } } }
          .to change { question.reload.message }.from("Foo").to("Bar")

        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe "DELETE /questions/<question_id>" do
    context "when the current_user is the question owner" do
      it "deleted the question in params" do
        user = create(:user)
        sign_in user

        question = create(:question, user: user)

        expect { delete question_url(question) }.to change { Question.count }.by(-1)

        expect(response).to have_http_status(:ok)
      end
    end

    context "when the current_user is the question owner" do
      it "deleted the question in params" do
        user = create(:user)
        sign_in user

        question = create(:question)

        expect { delete question_url(question) }.not_to change(Question, :count)

        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end
