require 'rails_helper'

describe User, type: :model do
  describe "#owner_of?" do
    context "when user is the owner of the object" do
      it "returns true" do
        user = create(:user)
        object = create(:question, user: user)

        expect(user.owner_of?(object)).to be true
      end
    end

    context "when user is not the owner of the object" do
      it "returns false" do
        user = create(:user)
        object = create(:question)

        expect(user.owner_of?(object)).to be false
      end
    end
  end
end
