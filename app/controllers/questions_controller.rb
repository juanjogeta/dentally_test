class QuestionsController < ApplicationController
  before_action :authenticate_user!, except: %i[index show]
  before_action :set_question, only: %i[show edit update destroy]

  def index
    @questions = Question.order(:created_at).page(params[:page]).per(10)
  end

  def show;end

  def new
    @question = Question.new
  end

  def create
    question = Question.new(question_params.merge(user: current_user))

    if question.save
      flash[:success] = "Question created succesfully"
      redirect_to question_path(question), status: :created
    else
      flash[:error] = question.errors.full_messages
      redirect_to new_question_path, status: :unprocessable_entity
    end
  end

  def edit;end

  def update
    @question.errors.add(:base, "You're not the owner of this question") unless current_user.owner_of?(@question)

    if @question.errors.empty? && @question.update(question_params)
      flash[:success] = "Question updated succesfully"
      redirect_to question_path(@question), status: :ok
    else
      flash[:error] = @question.errors.full_messages
      redirect_to edit_question_path(@question), status: :unprocessable_entity
    end
  end

  def destroy
    @question.errors.add(:base, "You're not the owner of this question") unless current_user.owner_of?(@question)

    if @question.errors.empty?
      @question.destroy!
      flash[:success] = "Question deleted succesfully"
      redirect_to questions_path, status: :ok
    else
      flash[:error] = @question.errors.full_messages
      redirect_to edit_question_path(@question), status: :unprocessable_entity
    end
  end

  private

  def question_params
    params.require(:question).permit(:message)
  end

  def set_question
    @question = Question.find(params[:id])
  end
end
