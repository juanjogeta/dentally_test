class AnswersController < ApplicationController
  before_action :authenticate_user!

  def new
    question = Question.find(params[:question_id])
    @answer = question.answers.new
  end

  def create
    answer = Answer.new(answer_params.merge(user: current_user, question_id: params[:question_id]))

    if answer.save
      flash[:success] = "Answer created succesfully"
      redirect_to question_path(answer.question), status: :created
    else
      flash[:error] = answer.errors.full_messages
      redirect_to new_question_answer_path(answer.question), status: :unprocessable_entity
    end
  end

  private

  def answer_params
    params.require(:answer).permit(:message)
  end
end
