class Answer < ApplicationRecord
  belongs_to :user
  belongs_to :question

  validates :message, presence: true
end
