class Question < ApplicationRecord
  belongs_to :user
  has_many :answers

  validates :message, presence: true
end
