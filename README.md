Welcome to the Dentally test.

## Configuration steps

### Install all the gems
`bundle install`

### Run the server

`bundle exec rails server`

You should be able to access to `localhost:3000`

## Application

### Database structure
![](https://i.ibb.co/QpPK36c/diagram-dentally.png)

### Actions allowed without starting a session

#### `GET localhost:3000/questions`

See all the available questions.

![](https://i.ibb.co/4K7BPBw/Screenshot-2022-03-25-at-15-39-12.png)


#### `GET localhost:3000/questions/<question_id>`

See all the information related to a question including its answers.

![](https://i.ibb.co/gJvBsv9/Screenshot-2022-03-25-at-15-44-43.png)


### Actions allowed after starting a session

#### `POST localhost:3000/questions`

A signed in user can create a new question.

![](https://i.ibb.co/qpMjCBn/Screenshot-2022-03-25-at-15-39-21.png)


#### `PUT/PATCH localhost:3000/questions/<question_id>`

A signed in user can edit a question if the user is the owner of the question.

![](https://i.ibb.co/dfzgL0n/Screenshot-2022-03-25-at-15-39-44.png)


#### `DELETE localhost:3000/questions/<question_id>`

A signed in user can delete a question if the user is the owner of the question.

![](https://i.ibb.co/C7BSzgw/Screenshot-2022-03-25-at-15-46-22.png)


#### `POST localhost:3000/questions/<question_id>/answers`

A signed in user can create a new answer related to a question.

![](https://i.ibb.co/b5xtNYY/Screenshot-2022-03-25-at-15-48-01.png)

### Extra info

* Pagination with Kaminari gem.
* Tests done with Rspec gem.
