class CreateQuestions < ActiveRecord::Migration[6.1]
  def change
    create_table :questions do |t|
      t.references :user, index: true, null: false, foreign_key: true
      t.text :message, null: false
      
      t.timestamps
    end
  end
end
